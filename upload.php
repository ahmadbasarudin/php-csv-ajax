
<?php
    /*
    |--------------------------------------------------------------------------
    | upload csv model
    |--------------------------------------------------------------------------
    |csv model upload
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

	require_once("class/class.csv.php");
	error_reporting(0);
	$reponse['result'] = "error";
	$reponse['desc'] = "tidak ada eksekusi";

	require_once("class/class.csv.php");

	$allowFile = array('text/csv','application/csv');
	if(in_array($_FILES["file"]["type"],$allowFile)) 
	{
		if ($_FILES["file"]["error"] > 0) {
			$reponse['result']  = "error";
			$reponse['desc'] = "error : ".$_FILES["file"]["error"];
		} 
	  	else 
	  	{
			$csv = new CSV("testfile");
			$csv->readCSV($_FILES["file"]["tmp_name"]);

			$data = array();
			$totalRows = $csv->totalRows();
			$totalCols = $csv->totalCols();
			for($row=0; $row<$totalRows; $row++) {
			  for($col=0; $col<$totalCols; $col++) {
			  	$data[$row][$col] =$csv->getRowCol($row, $col);
			  }  
			}
			$reponse['result']  = "success";
			$reponse['desc'] = "berhasil dibaca";
			$reponse['data'] = $data;
	  	}
	} 
	else 
	{
		$reponse['result']  = "error";
		$reponse['desc'] = "tipe file salah";
	}
	echo json_encode($reponse);
?> 