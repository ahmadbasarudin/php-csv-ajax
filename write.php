<?php

require_once("class/class.csv.php");
//Start the CSV object and provide a filename
$csv = new CSV("testfile");
 
//create the header row
$headerRow = array("ID", "NAME", "POSITION");
$csv->headerColumns($headerRow);
 
//add some data
$data = array(1, "John Doe", "Sales");
$csv->addRow($data);
$dataTwo = array(2, "Bobby Bush", "Admin");
$csv->addRow($data);
 
//export the data to a CSV file
$csv->export();
?>