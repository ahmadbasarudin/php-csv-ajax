<?php
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en-US">
<title>PHP UPLOAD CSV +ajax with progress bar!</title>
<meta charset="utf-8">
<meta name="Keywords" content="Multi Image upload in php and ajax with progress bar">
<meta name="Description" content="Multi Image upload in php and ajax with progress bar">
<body>
<div style="width:60%; margin:0 auto;">
<h3>php upload csv+ajax with progress bar! </h3>
<div style="border:1px solid; padding:10px 10px 10px 10px; float:left; width:800px;">
<form enctype="multipart/form-data" id="myform" name="myform">  
	 <b>Choose csv to upload.</b><br/>
		<input type="file"  name="file[]" id="image" multiple />
		<br>
	 <font color=red>Note: Choose only .csv ..!!</font><br/>  
		<input type="button" value="Upload images" class="upload" />
</form>
<div id="progBar" style="display:none;">
 <span id="fname" style="color:red;"></span>
	<progress value="0" max="100" style="width:750px; "></progress><span id="prog" style="font-weight:bold;">0%</span>
</div>
<div id="alertMsg" style="font-size:16px; color:blue; display:none;"></div>
<h3>List of uploaded files</h3>
	</div>
 <p id="showData"></p>
 </div>
	<script src="js/jquery.min.js"></script>
	<script src="js/upload.js"></script>
 </body>
</html>