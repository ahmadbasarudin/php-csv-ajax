$(function() {

	// call click event
	$(".upload").click(function() 
	{
		var fl= document.getElementById('image');
		if(fl.files.length==0) 
		{
			flashMsg("Silahkan pilih file csv."); 
			return false;
		}
		$(this).attr('disabled','disabled');
		sendRequest(0,fl);
	});

	// Send request to server
	function sendRequest(i,fl)
	{
		var ln= fl.files.length;
		if(i<ln) 
		{
			var curFileName = fl.files.item(i).name;
			$("#fname").html(curFileName);
			$("#progBar").show();
			var formData = new FormData();
			var fileD = document.getElementById('image').files[i];
		 	formData.append('file', fileD);
		 	$.ajax({
				url: 'upload.php',
				type: 'POST',
				data: formData,
				processData: false, 
				contentType: false,
				dataType: 'json',
				xhr: function() {
								var myXhr = $.ajaxSettings.xhr();
								if(myXhr.upload){
										myXhr.upload.addEventListener('progress',progress, false);
								}
								return myXhr;
						},
	            //jika complete maka
	            complete:
	                function(data,status)
	                {
	                 return false;
	                },
	            success:
	                function(msg,status)
	                {
	                    //alert(msg.result);
	                    if(msg.result == 'success')
	                    {
	                        
							CreateTableFromJSON(msg.data);

	                    }

						flashMsg(msg.desc); 
						$("#progBar").hide();
						resetProgressBar();
						i++;
						sendRequest(i,fl)

	                },
	            //untuk sementara tidak digunakan
	            error:
	                function(msg)
	                {
	                    
						alert(msg.status+" error occurred to upload image!");
						window.location.href=window.location.href;
	                }
	        });//end of ajax
	    }
	    else 
	    {
			 $("#myform")[0].reset();
			 $(".upload").removeAttr('disabled');
	 	}
	}
 
	// Proress bar
	function progress(e)
	{
		if(e.lengthComputable)
		{
			$('progress').attr({value:e.loaded,max:e.total});
			var percentage = (e.loaded / e.total) * 100;
			$('#prog').html(percentage.toFixed(0)+'%');
		}
	}

	//Reset progress bar
	function resetProgressBar() {
		$('#prog').html('0%');
		$('progress').attr({value:0,max:100});
	}
		
	// Flash message 
	function flashMsg(msg) {
		$("#alertMsg").fadeIn().html(msg).fadeOut(2000);
	}
 
	function CreateTableFromJSON(mydata) {

			// EXTRACT VALUE FOR HTML HEADER. 
			// ('Book ID', 'Book Name', 'Category' and 'Price')
			var col = [];
			for (var i = 0; i < mydata.length; i++) {
					for (var key in mydata[i]) {
							if (col.indexOf(key) === -1) {
									col.push(key);
							}
					}
			}

			// CREATE DYNAMIC TABLE.
			var table = document.createElement("table");

			// CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

			var tr = table.insertRow(-1);                   // TABLE ROW.

			// ADD JSON DATA TO THE TABLE AS ROWS.
			for (var i = 0; i < mydata.length; i++) {

					tr = table.insertRow(-1);

					for (var j = 0; j < col.length; j++) {
							var tabCell = tr.insertCell(-1);
							tabCell.innerHTML = mydata[i][col[j]];
					}
			}

			// FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
			var divContainer = document.getElementById("showData");
			divContainer.innerHTML = "";
			divContainer.appendChild(table);
	}



});
